# Data

This repository contains the data of the ICISSP paper titled:

"RoomKey: Extracting a Volatile Key with Information from the Local
WiFi Environment Reconstructable within a Designated Area"

## Desk 
Contains anonymized scan data from a square-meter desk.

## Office's 
Contain anonymized scan data of the respective offices.

